
'use strict';


class GuidGenerator {
  static lookup = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102];
  
  static create () {
    const r = new Array(36);
    r[8] = 45;
    r[13] = 45;
    r[14] = 52;
    r[18] = 45;
    r[19] = GuidGenerator.lookup[(Math.random()*16|0)&0x3|0x8];
    r[23] = 45;
    for(let i = 0; i < 8; ++i) {
      r[i] = GuidGenerator.lookup[(Math.random()*16|0)];
    }
  	for(let i = 9; i < 13; ++i) {
      r[i] = GuidGenerator.lookup[(Math.random()*16|0)];
    }
  	for(let i = 15; i < 18; ++i) {
      r[i] = GuidGenerator.lookup[(Math.random()*16|0)];
    }
  	for(let i = 20; i < 23; ++i) {
      r[i] = GuidGenerator.lookup[(Math.random()*16|0)];
    }
  	for(let i = 24; i < 36; ++i) {
      r[i] = GuidGenerator.lookup[(Math.random()*16|0)];
    }
  	return String.fromCharCode(...r);
  }
}


module.exports = GuidGenerator;


'use strict';

const CoreProtocolConst = require('./core-protocol/core-protocol-const');


class ActionRequest {
  constructor(requests = []) {
    this.msgId = CoreProtocolConst.REQUEST;
    this.id = '';
    this.requests = requests;
    this.isServiceAction = false;
  }
  
  setIds(requestId, sessionId) {
    this.id = requestId;
    this.requests.forEach((request) => {
      request.sessionId = sessionId;
    });
  }
  
  add(name, index, ...params) {
    this.requests.push({
      name: name,
      index: index,
      sessionId: undefined,
      params: [...params]
    });
    return this;
  }
}


module.exports = ActionRequest;

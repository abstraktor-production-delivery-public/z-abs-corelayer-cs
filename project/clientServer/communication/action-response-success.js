
'use strict';


class ActionResponseSuccess {
  constructor(name, index, data) {
    this.name = name;
    this.index = index;
    this.data = data;
    this.isServiceAction = false;
    this.result = {
      code: 'success'
    };
  }
}


module.exports = ActionResponseSuccess;


'use strict';


class ActionResponseError {
  constructor(name, index, msg) {
    this.name = name;
    this.index = index;
    this.isServiceAction = false;
    this.result = {
      code: 'error',
      msg: msg
    };
  }
}


module.exports = ActionResponseError;

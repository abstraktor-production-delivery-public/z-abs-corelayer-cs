
'use strict';


class TextCache {
  constructor(name) {
    this.name = name;
    this.externalId = -1;
    this.externalText = new Map([['', 0]]);
    this.externalNumber = [];
    this.internalId = 1;
    this.internalText = new Map([['', 0]]);
    this.internalNumber = [];
  }
  
  getText(id) {
    if(id >= 0) {
      return this.externalNumber[id];
    }
    else {
      return this.internalNumber[id];
    }
  }
  
  set(id, text) {
    this.externalText.set(text, id);
    this.externalNumber.push(text);
  }
  
  setExternal(text, cachedTexts) {
    if(this.externalText.has(text)) {
      const id = this.externalText.get(text);
      return id;
    }
    else {
      /*if(this.externalId >= 100) {
        this.externalId = 0;
      }*/
      const id = ++this.externalId;
      this.externalText.set(text, id);
      //console.log(id, text);
      this.externalNumber.push(text);
      ++cachedTexts.nbr;
      cachedTexts.texts.push({
        id: id, 
        text: text
      });
      return id;
    }
  }
  
  setInternal(text) {
    if(this.externalText.has(text)) {
      const id = this.externalText.get(text);
      return id;
    }
    else if(this.internalText.has(text)) {
      const id = this.internalText.get(text);
      return id;
    }
    else {
      const id = --this.internalId;
      this.internalText.set(text, id);
      this.internalNumber.push(text);
      return id;
    }
  }
  
  clear() {
    this.externalId = 0;
    this.externalText.clear();
    this.externalNumber = [];
    this.internalId = 0;
    this.internalText.clear();
    this.internalNumber = [];
  }
}


module.exports = TextCache;

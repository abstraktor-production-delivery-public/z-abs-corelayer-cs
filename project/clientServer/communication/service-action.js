
'use strict';

const CoreProtocolConst = require('./core-protocol/core-protocol-const');
const ActionRequest = require('./action-request');


class ServiceAction {
  constructor(serviceName, ...params) {
    this.msgId = CoreProtocolConst.REQUEST;
    this.serviceName = serviceName;
    this.actionRequest = new ActionRequest().add(this.constructor.name.substring(13), 0, ...params);
    this.actionRequest.isServiceAction = true;
    this.responses = [];
  };
  
  setIds(requestId, sessionId) {
    this.actionRequest.setIds(requestId, sessionId);
  }
  
  setResponses(responses) {
    if(responses) {
      this.responses = responses;
    }
  }
}


module.exports = ServiceAction;

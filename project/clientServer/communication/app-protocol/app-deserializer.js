
'use strict';


class AppDeserializer {
  constructor(decoder, decoderCahced) {
    this.decoder = decoder;
    this.decoderCahced = decoderCahced;
    this.dataDecoders = new Map();
  }
  
  do(msgId, buffer, isServiceAction) {
    try {
      return this._do(msgId, buffer, isServiceAction);
    }
    catch(err) {
      ddb.error('AppDeserializer.do ', msgId, err);
      //this.decoder.clear();
      return null;
    }
  }
  
  clear() {
    this.decoder.clear();
  }
  
  _do(msgId, buffer, isServiceAction) {
    const deserializerData = this.dataDecoders.get(msgId);
    if(!deserializerData) {
      return null;
    }
    else {
      this.decoder.add(buffer);
      if(this.decoder.has(buffer.byteLength)) {
        try {
          const msg = deserializerData.deserializer.do(msgId, deserializerData.onRealtimeName, this.decoder, isServiceAction);
          if(msg) {
            this.decoder.got();
            return msg;
          }
        }
        catch(err) {
          ddb.error(`AppDeserializer msgId: ${msgId} error:`, err);
        }
      }
      return null;
    } 
  }
  
  doMsg(msg) {
    const deserializerData = this.dataDecoders.get(msg.msgId);
    if(deserializerData) {
      msg.onRealtimeName = deserializerData.onRealtimeName;
    }
  }
  
  doCachedText(cachedTextBuffer) {
    const decoder = this.decoderCahced;
    decoder.add(cachedTextBuffer);
    decoder.has(cachedTextBuffer.byteLength);
    const nbrOfCachedTexts = decoder.getUint8();
    for(let i = 0; i < nbrOfCachedTexts; ++i) {
      const ct = decoder.getCt();
      const text = decoder.getString();
      this.decoder.textCache.set(ct, text);
    }
    decoder.got();
  }
  
  register(id, name, deserializer) {
    if(!this.dataDecoders.has(id)) {
      this.dataDecoders.set(id, {
        onRealtimeName: name ? `onRealtime${name}` : null,
        name: deserializer ? deserializer.constructor.name : (name ? name : 'unknown'),
        deserializer: deserializer
      });
    }
    else {
      const deserializerData = this.dataDecoders.get(id);
      ddb.error(`App deserializer: ${deserializerData.name} have already registered the id: ${id}`);
    }
  }
}


module.exports = AppDeserializer;

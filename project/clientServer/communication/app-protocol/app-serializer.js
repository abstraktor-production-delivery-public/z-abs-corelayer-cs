
'use strict';


class AppSerializer {
  constructor() {
    this.appSerializers = new Map();
    this.encoder = null;
  }
  
  init(encoder) {
    this.encoder = encoder;
  }
  
  do(msgId, msg, cachedTexts) {
    try {
      const buffer = this._do(msgId, msg, cachedTexts);
      this.encoder.clear();
      return buffer;
    }
    catch(err) {
      ddb.error('AppSerializer.do ', err);
    }
  }
  
  clear() {
    this.textCache.clear();
  }
  
  _do(msgId, msg, cachedTexts) {
    const serializerData = this.appSerializers.get(msgId);
    if(!serializerData) {
      return null;
    }
    else {
      try {
        return serializerData.appSerializer.do(msg, cachedTexts);
      }
      catch(err) {
        ddb.error(`AppSerializer msgId: ${msg.msgId} error:`, err);
      }
      return null;
    } 
  }
  
  _register(msgId, appSerializer) {
    appSerializer.init(this.encoder);
    this.appSerializers.set(msgId, {
      name: appSerializer ? appSerializer.constructor.name : 'null',
      appSerializer: appSerializer
    });
  }
  
  register(msgId, appSerializer) {
    if(!this.appSerializers.has(msgId)) {
      this._register(msgId, appSerializer);
    }
    else {
      const serializerData = this.appSerializers.get(msgId);
      debugger;
      ddb.error(`App serializer: ${serializerData.name} have already registered the msgId: ${msgId}`);
    }
  }
}


module.exports = AppSerializer;

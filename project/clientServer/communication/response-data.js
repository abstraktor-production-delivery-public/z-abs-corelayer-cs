
'use strict';

class ActionData {
  constructor(result, data) {
    this.result = result;
    this.data = data;
  }
  
  isSuccess() {
    return 'success' === this.result.code;
  }
}


module.exports = ActionData;


'use strict';


class DeserializerMessageInit {
  constructor(jsonFunc) {
    this.jsonFunc = jsonFunc;
  }
  
  do(msgId, onRealtimeName, decoder, isServiceAction) {
    const port = decoder.getUint16();
    return {
      msgId,
      onRealtimeName,
      port
    };
  }
}


module.exports = DeserializerMessageInit;

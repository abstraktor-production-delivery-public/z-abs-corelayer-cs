
'use strict';


class DeserializerMessageResponse {
  constructor(jsonFunc) {
    this.jsonFunc = jsonFunc;
  }
  
  do(msgId, onRealtimeName, decoder, isServiceAction) {
    const id = decoder.getGuid();
    const nbrOfResponses = decoder.getUint16();
    const responses = [];
    this._getResponses(decoder, decoder.textCache, nbrOfResponses, responses, isServiceAction);
    return {
      msgId,
      onRealtimeName,
      id,
      responses: responses
    };
  }
  
  _getResponse(decoder, textCache, responses, isServiceAction) {
    const index = decoder.getUint16();
    const name = isServiceAction ? decoder.getString() : decoder.getCtString();
    
    const resultId = decoder.getUint8();
    const response = {
      name,
      index,
      result: null,
      data: null
    };
    responses.push(response);
    if(0 === resultId) {
      const msg = decoder.getString();
      response.result = {
        code: 'error',
        msg: msg
      }
    }
    else if(1 === resultId) {
      response.result = {
        code: 'success'
      }
    }
    else if(2 === resultId) {
      response.result = {
        code: 'success'
      }
      response.data = JSON.parse(decoder.getString(), this.jsonFunc);
    }
  }
  
  _getResponses(decoder, textCache, nbrOfResponses, responses, isServiceAction) {
    for(let i = 0; i < nbrOfResponses; ++i) {
      this._getResponse(decoder, textCache, responses, isServiceAction);
    }
  }
}


module.exports = DeserializerMessageResponse;

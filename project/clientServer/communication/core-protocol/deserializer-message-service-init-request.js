
'use strict';


class DeserializerMessageServiceInitRequest {
  constructor(jsonFunc) {
    this.jsonFunc = jsonFunc;
  }
  
  do(msgId, onRealtimeName, decoder, isServiceAction) {
    const nodeName = decoder.getString();
    const nodeId = decoder.getGuid();
    const nodeType = decoder.getUint8();
    return {
      msgId,
      nodeName,
      nodeId,
      nodeType
    };
  }
}


module.exports = DeserializerMessageServiceInitRequest;

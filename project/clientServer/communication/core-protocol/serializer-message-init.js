
'use strict';


class SerializerMessageInit {
  constructor() {
    this.encoder = null;
  }
  
  init(encoder) {
    this.encoder = encoder;
  }
  
  do(msg, cachedTexts) {
    const encoder = this.encoder;
    const buffer = encoder.createBuffer(2);
    encoder.setUint16(msg.port);
    return buffer;
  }
}


module.exports = SerializerMessageInit;

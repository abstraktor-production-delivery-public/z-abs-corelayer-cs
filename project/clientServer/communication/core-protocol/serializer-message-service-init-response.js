
'use strict';


class SerializerMessageServiceInitResponse {
  constructor(jsonFunc) {
    this.jsonFunc = jsonFunc;
    this.encoder = null;
    this.params = [];
  }
  
  init(encoder) {
    this.encoder = encoder;
  }
  
  do(msg, cachedTexts) {
    const encoder = this.encoder;
    const size = this._calculateData(msg);
    const buffer = encoder.createBuffer(size);
    encoder.setGuid(msg.id);
  }
  
  _calculateData(msg) {
    const encoder = this.encoder;
  }
}


module.exports = SerializerMessageServiceInitResponse;

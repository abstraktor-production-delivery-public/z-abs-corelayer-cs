
'use strict';


class CoreMessage {
  constructor(msgId, hasEnvelope, isBinary, hasBuffers, hasTextCache, isServiceAction, envelope, msg, bin, cachedTextBuffer, dataBuffers) {
    this.msgId = msgId;
    this.hasEnvelope = hasEnvelope;
    this.isBinary = isBinary;
    this.hasBuffers = hasBuffers;
    this.hasTextCache = hasTextCache;
    this.isServiceAction = isServiceAction;
    this.envelope = envelope;
    this.msg = msg;
    this.bin = bin;
    this.cachedTextBuffer = cachedTextBuffer;
    this.dataBuffers = dataBuffers;
  }
}


module.exports = CoreMessage;

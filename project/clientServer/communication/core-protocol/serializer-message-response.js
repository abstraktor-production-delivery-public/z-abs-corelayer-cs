
'use strict';

const EncoderConst = require('./encoder-const');


class SerializerMessageResponse {
  static SIZE = 2 + 16;
  
  constructor(jsonFunc) {
    this.jsonFunc = jsonFunc;
    this.encoder = null;
    this.datas = [];
  }
  
  init(encoder) {
    this.encoder = encoder;
  }
  
  do(msg, cachedTexts) {
    this.datas = [];
    const encoder = this.encoder;
    const size = this._calculateData(msg);
    const buffer = encoder.createBuffer(size);
    encoder.setGuid(msg.id);
    encoder.setUint16(msg.responses.length);
    this._writeResponses(msg, cachedTexts);
    return buffer;
  }
  
  _calculateData(msg) {
    const encoder = this.encoder;
    let size = SerializerMessageResponse.SIZE;
    for(let i = 0; i < msg.responses.length; ++i) {
      const response = msg.responses[i];
      size += EncoderConst.Uint16Size + (!msg.isServiceAction ? EncoderConst.CtSize : encoder.getStringBytes(response.name)) + EncoderConst.Uint8Size;
      if('success' === response.result.code) {
        if(response.data) {
          const data = JSON.stringify(response.data, this.jsonFunc);
          size += encoder.getStringBytes(data);
          this.datas.push(data);
        }
        else {
          this.datas.push(null);
        }
      }
      else {
        size += encoder.getStringBytes(response.result.msg);
      }
    }
    return size;
  }
    
  _writeResponse(response, index, cachedTexts, isServiceAction) {
    const encoder = this.encoder;
    encoder.setUint16(response.index);
    if(!isServiceAction) {
      encoder.setCtString(response.name, cachedTexts);
    }
    else {
      encoder.setString(response.name);
    }
    if('success' === response.result.code) {
      if(response.data) {
        encoder.setUint8(2);
        const data = this.datas[index];
        encoder.setString(data);
      }
      else {
        encoder.setUint8(1);
      }
    }
    else {
      encoder.setUint8(0);
      encoder.setString(response.result.msg);
    }
  }
  
  _writeResponses(msg, cachedTexts) {
    msg.responses.forEach((response, index) => {
      this._writeResponse(response, index, cachedTexts, msg.isServiceAction);
    });
  }
}


module.exports = SerializerMessageResponse;

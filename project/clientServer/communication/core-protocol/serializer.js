
'use strict';

const CoreMessage = require('./core-message');
const EncoderConst = require('./encoder-const');
const SerializerMessageInit = require('./serializer-message-init');
const SerializerMessageRequest = require('./serializer-message-request');
const SerializerMessageResponse = require('./serializer-message-response');
const SerializerMessageServiceInitRequest = require('./serializer-message-service-init-request');
const CoreProtocolConst = require('./core-protocol-const');
const AppSerializer = require('../app-protocol/app-serializer');
const TextCache = require('../cache/text-cache');


//        0                   1                   2                   3
//        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//       |      0        |     flags     |            message id         |
//       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//       |             data size                                         |
//       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//       |  nbrOfBuffers |       data buffer size[i]...                  |
//       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//       |                           data buffers                        |
//       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//       |  
//       +-+-+-+-+-+-+-+-+-+-+-+-+-
//
//       total size: UInt32 - 
//       flags: UInt8 - b0: ENVELOPE (FALSE/TRUE) | b1: DATA (JSON/BIN) | b2: DATA-BUFFERS (FALSE/TRUE)
//       data size: UInt32 - 
//       nbrOfBuffers: UInt8 (OPTIONAL)
//       data buffer size: UInt32 * nbrOfBuffers (OPTIONAL)
//       data buffer: UInt8 * [data size]
class Serializer {
  constructor(Encoder, textCache) {
    this.appSerializer = new AppSerializer();
    this.textCache = textCache ? textCache : new TextCache('SERIALIZER');
    this.encoder = new Encoder(this.textCache);
    this.cachedTexts = {
      nbr: 0,
      texts: []
    };
    this.appSerializer.init(this.encoder);
    this.appSerializer.register(CoreProtocolConst.INIT, new SerializerMessageInit());
    this.appSerializer.register(CoreProtocolConst.REQUEST, new SerializerMessageRequest(Serializer.jsonFunc));
    this.appSerializer.register(CoreProtocolConst.RESPONSE, new SerializerMessageResponse(Serializer.jsonFunc));
    this.appSerializer.register(CoreProtocolConst.SERVICE_INIT_REQUEST, new SerializerMessageServiceInitRequest(Serializer.jsonFunc));
  }
  
  register(msgId, appSerializer) {
     this.appSerializer.register(msgId, appSerializer);
  }
  
  do(msg, dataBuffers, envelope) {
    //console.log('SERIALIZE:', msg.msgId);
    const buffers = [];
    try {
      const msgId = msg.msgId ? ('number' === typeof msg.msgId ? msg.msgId : 0) : 0;
      this._do(buffers, envelope, msg, dataBuffers, msgId);
      //console.log('SERIALIZE - DONE', buffers);
      return buffers;
    }
    catch(err) {
      ddb.error(err, msg);
      throw err;
    }
  }
  
  clear() {
    this.appSerializer.clear();
  }
  
  static jsonFunc = (key, value) => {
    if('bigint' === typeof value) {
      return {
        bigint__: true,
        value: value.toString()
      };
    }
    else {
      return value;
    }
  }
  
  _do(buffers, envelope, msg, dataBuffers, msgId) {
    const encoder = this.encoder;
    let sizeData = 0;
    let msgDataBuffer = null;
    let sizeTextCache = 0;
    const cachedTexts = this.cachedTexts;
    cachedTexts.nbr = 0;
    cachedTexts.texts = [];
    const nbrOfBuffers = dataBuffers ? dataBuffers.length : 0;
    if(!envelope) {
      envelope = msg.envelope;
    }
    const hasEnvelope = !!envelope;
    let isBinary = !!msg.isBinary;
    const hasBuffers = 0 !== nbrOfBuffers;
    let hasTextCache = false;
    const isServiceAction = !!msg.isServiceAction;
    if(isBinary) {
      msgDataBuffer = msg.bin;
      sizeData += encoder.calculateBytesFromBin(msgDataBuffer);
    }
    else {
      if(0 !== msgId) {
        msgDataBuffer = this.appSerializer.do(msgId, msg instanceof CoreMessage ? msg.msg : msg, cachedTexts);
        hasTextCache = 0 !== cachedTexts.nbr;
        if(hasTextCache) {
          sizeTextCache += 1;
          for(let i = 0; i < cachedTexts.texts.length; ++i) {
            const text = cachedTexts.texts[i].text;
            if(text) {
              sizeTextCache += 2;
              sizeTextCache += encoder.getStringBytes(text);
            }
          }
        }
      }
      if(!msgDataBuffer) {
        msgDataBuffer = JSON.stringify(msg, Serializer.jsonFunc);
        sizeData += encoder.calculateBytesFromString(msgDataBuffer);
      }
      else {
        isBinary = true;
        sizeData += encoder.calculateBytesFromBin(msgDataBuffer);
      }
    }
    this._encode(true, encoder, hasEnvelope, isBinary, hasBuffers, hasTextCache, isServiceAction, envelope, msgId, sizeTextCache, sizeData, msgDataBuffer, msg.cachedTextBuffer, cachedTexts, nbrOfBuffers, dataBuffers);
    buffers.push(encoder.createBuffer(encoder.offset));
    this._encode(false, encoder, hasEnvelope, isBinary, hasBuffers, hasTextCache, isServiceAction, envelope, msgId, sizeTextCache, sizeData, msgDataBuffer, msg.cachedTextBuffer, cachedTexts, nbrOfBuffers, dataBuffers);
    if(hasBuffers) {
      dataBuffers.forEach((dataBuffer) => {
        buffers.push(dataBuffer);                        // DATA buffers
      });
    }
    encoder.clear();
  }
  
  _encode(doCalculate, encoder, hasEnvelope, isBinary, hasBuffers, hasTextCache, isServiceAction, envelope, msgId, sizeTextCache, sizeData, msgDataBuffer, cachedTextBuffer, cachedTexts, nbrOfBuffers, dataBuffers) {
    encoder.calculate(doCalculate);
    encoder.setUint8(0);
    encoder.setBool1_0(hasEnvelope, false);
    encoder.setBool1_1(isBinary, false);
    encoder.setBool1_2(hasBuffers, false);
    encoder.setBool1_3(hasTextCache || cachedTextBuffer, false);
    encoder.setBool1_4(isServiceAction, true);
    encoder.setUint16(msgId);
    if(hasEnvelope) {
      encoder.setString(envelope.serviceName);
      encoder.setGuidArray(envelope.routes);
      encoder.setGuidArray(envelope.recordRoutes);
    }
    if(cachedTextBuffer) {
      const size = encoder.calculateBytesFromBin(cachedTextBuffer);
      encoder.setDynamicBytes(size);
      encoder.setRawBinary(cachedTextBuffer, size);
    }
    else if(hasTextCache) {
      encoder.setDynamicBytes(sizeTextCache);
      encoder.setUint8(cachedTexts.nbr); // cached texts
      for(let i = 0; i < cachedTexts.texts.length; ++i) {
        const text = cachedTexts.texts[i];
        if(text.text) {
          encoder.setUint16(text.id);
          encoder.setString(text.text);
        }
      }
    }
    encoder.setDynamicBytes(sizeData);
    if(isBinary) { // data
      encoder.setRawBinary(msgDataBuffer, sizeData);
    }
    else {
      encoder.setRawString(msgDataBuffer, sizeData);
    }
    if(hasBuffers) {
      encoder.setUint8(nbrOfBuffers);  // nbr of data buffers
      dataBuffers.forEach((dataBuffer) => {
        encoder.setUint32(dataBuffer.byteLength); // size of data buffer[i]
      });
    }
  }
}


module.exports = Serializer;

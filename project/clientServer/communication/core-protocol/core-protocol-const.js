
'use strict';


class CoreProtocolConst {
  static REQUEST = 1;
  static RESPONSE = 2;
  static WORKER_INIT_REQUEST = 3;
  static WORKER_INIT_RESPONSE = 4;
  static PERSISTENT_INIT_REQUEST = 5;
  static PERSISTENT_INIT_RESPONSE = 6;
  static PERSISTENT_PUBLISH = 7;
  static SERVICE_INIT_REQUEST = 8;
  static SERVICE_INIT_RESPONSE = 9;
  static SERVICE_OFFER_ON = 10;
  static SERVICE_OFFER_OFF = 11;
  static APP = 999;
}


module.exports = CoreProtocolConst;

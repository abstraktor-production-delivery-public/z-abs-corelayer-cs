
'use strict';


class DeserializerMessageRequest {
  constructor(jsonFunc) {
    this.jsonFunc = jsonFunc;
  }
  
  do(msgId, onRealtimeName, decoder, isServiceAction) {
    const id = decoder.getGuid();
    const nbrOfRequests = decoder.getUint16();
    const requests = [];
    this._getRequests(decoder, decoder.textCache, nbrOfRequests, requests, isServiceAction);
    return {
      msgId,
      id,
      requests: requests
    };
  }
  
  _getRequest(decoder, textCache, requests, isServiceAction) {
    const index = decoder.getUint16();
    const name = isServiceAction ? decoder.getString() : decoder.getCtString();
    const params = JSON.parse(decoder.getString(), this.jsonFunc);
    const hasSessionId = !!decoder.getUint1_0(true);
    const sessionId = hasSessionId ? decoder.getGuid() : undefined;
    const request = {
      index,
      name,
      params,
      sessionId
    };
    requests.push(request);
  }
  
  _getRequests(decoder, textCache, nbrOfRequests, requests, isServiceAction) {
    for(let i = 0; i < nbrOfRequests; ++i) {
      this._getRequest(decoder, textCache, requests, isServiceAction);
    }
  }
}


module.exports = DeserializerMessageRequest;

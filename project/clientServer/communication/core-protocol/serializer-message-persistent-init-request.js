
'use strict';


class SerializerMessagePersistentInitRequest {
  constructor(jsonFunc) {
    this.jsonFunc = jsonFunc;
    this.encoder = null;
    this.params = [];
  }
  
  init(encoder) {
    this.encoder = encoder;
  }
  
  do(msg, cachedTexts) {
    const encoder = this.encoder;
    const size = this._calculateData(msg);
    const buffer = encoder.createBuffer(size);
    encoder.setString(msg.nodeName);
    encoder.setGuid(msg.nodeId);
    encoder.setUint8(msg.nodeType);
    return buffer;
  }
  
  _calculateData(msg) {
    const encoder = this.encoder;
    return encoder.getStringBytes(msg.nodeName) + EncoderConst.GuidSize + EncoderConst.Uint8Size;
  }
}


module.exports = SerializerMessagePersistentInitRequest;

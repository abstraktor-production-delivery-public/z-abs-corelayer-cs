
'use strict';


class EncoderConst {
  static GuidSize = 16;
  static Int8Size = 1;
  static Int16Size = 2;
  static Int32Size = 4;
  static Int64Size = 8;
  static Uint8Size = 1;
  static Uint16Size = 2;
  static Uint32Size = 4;
  static Uint64Size = 8;
  static Float32Size = 4;
  static Float64Size = 8;
  static CtSize = 2;
  static Uint8ArraySize = 2;
}


module.exports = EncoderConst;

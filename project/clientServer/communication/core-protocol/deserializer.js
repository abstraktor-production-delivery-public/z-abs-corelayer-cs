
'use strict';

const CoreMessage = require('./core-message');
const Envelope = require('./envelope');


class Deserializer {
  static HEADER = 0;
  static ENVELOPE_SERVICE_NAMES_SIZE = 1;
  static ENVELOPE_SERVICE_NAMES = 2;
  static ENVELOPE_ROUTES_SIZE = 3;
  static ENVELOPE_ROUTES = 4;
  static ENVELOPE_RECORD_ROUTES_SIZE = 5;
  static ENVELOPE_RECORD_ROUTES = 6;
  static TEXT_CACHE_SIZE_1 = 7;
  static TEXT_CACHE_SIZE_2 = 8;
  static DATA_SIZE_1 = 9;
  static DATA_SIZE_2 = 10;
  static HEADER_BUFFERS_NBR = 11;
  static HEADER_BUFFERS_SIZES = 12;
  static DATA = 13;
  static TEXT_CACHE = 14;
  static DATA_BUFFERS = 15;
  static DONE = 16;
  
  constructor(Decoder, textCache) {
    this.decoder = new Decoder(textCache);
    this.state = Deserializer.HEADER;
    this.hasEnvelope = false;
    this.isBinary = false;
    this.hasBuffers = false;
    this.hasTextCache = false;
    this.isServiceAction = false;
    this.msgId = 0;
    this.dataSize = 0;
    this.envelopeServiceNamesSize = 0;
    this.envelopeRoutesSize = 0;
    this.envelopeRecordRoutesSize = 0;
    this.cachedTextSize = 0;
    this.dataBufferSizeIndex = 0;
    this.nbrOfDataBuffers = 0;
    this.dataBufferSizes = null;
    this.envelope = null;
    this.msg = null;
    this.bin = null;
    this.cachedTextBuffer = null
    this.dataBuffers = null;
  }
  
  clear() {
    this.state = Deserializer.HEADER;
    this.hasEnvelope = false;
    this.isBinary = false;
    this.hasBuffers = false;
    this.hasTextCache = false;
    this.isServiceAction = false;
    this.msgId = 0;
    this.dataSize = 0;
    this.envelopeServiceNamesSize = 0;
    this.envelopeRoutesSize = 0;
    this.envelopeRecordRoutesSize = 0;
    this.cachedTextSize = 0;
    this.dataBufferSizeIndex = 0;
    this.nbrOfDataBuffers = 0;
    this.dataBufferSizes = null;
    this.envelope = null;
    this.msg = null;
    this.bin = null;
    this.cachedTextBuffer = null
    this.dataBuffers = null;
  }
  
  totalClear() {
    this.clear();
  }
  
  do(buffer) {
    this.decoder.add(buffer);
    try {
      return this._do();
    }
    catch(err) {
      console.log('Deserializer.do:', err);
    }
  }
  
  static jsonFunc(key, value) {
    if('object' === typeof value) {
      if(value && value.bigint__) {
        return BigInt(value.value);
      }
    }
    return value;
  }
  
  _do() {
    const decoder = this.decoder;
    while(true) {
      switch(this.state) {
        case Deserializer.HEADER: {
          if(!decoder.has(4)) {
            return null;
          }
          const isProtocol = 0 === decoder.getUint8(0);
          if(!isProtocol) {
            const msg = decoder.parse(Deserializer.jsonFunc);
            let coreMessage = null;
            if(msg) {
              coreMessage = new CoreMessage(msg.msgId, false, false, false, false, false, null, msg, null, null, null);
            }
            this.clear();
            return coreMessage;
          }
          this.hasEnvelope = decoder.getBool1_0(false);
          this.isBinary = decoder.getBool1_1(false);
          this.hasBuffers = decoder.getBool1_2(false);
          this.hasTextCache = decoder.getBool1_3(false);
          this.isServiceAction = decoder.getBool1_4(true);
          this.msgId = decoder.getUint16();
          if(this.hasEnvelope) {
            this.envelope = new Envelope();
            this.state = Deserializer.ENVELOPE_SERVICE_NAMES_SIZE;
          }
          else if(this.hasTextCache) {
            this.state = Deserializer.TEXT_CACHE_SIZE_1;
          }
          else {
            this.state = Deserializer.DATA_SIZE_1;
          }
          decoder.got();
          break;
        }
        case Deserializer.ENVELOPE_SERVICE_NAMES_SIZE: {
          if(!decoder.has(1)) {
            return null;
          }
          this.envelopeServiceNamesSize = decoder.calculateDynamicBytes();
          this.state = Deserializer.ENVELOPE_SERVICE_NAMES;
          decoder.got();
          break;
        }
        case Deserializer.ENVELOPE_SERVICE_NAMES: {
          if(!decoder.has(this.envelopeServiceNamesSize)) {
            return null;
          }
          this.envelope.setServiceName(decoder.getString());
          this.state = Deserializer.ENVELOPE_ROUTES_SIZE;
          decoder.got();
          break;
        }
        case Deserializer.ENVELOPE_ROUTES_SIZE: {
          if(!decoder.has(1)) {
            return null;
          }
          this.envelopeRoutesSize = decoder.calculateDynamicBytes();
          this.state = Deserializer.ENVELOPE_ROUTES;
          decoder.got();
          break;
        }
       case Deserializer.ENVELOPE_ROUTES: {
          if(!decoder.has(this.envelopeRoutesSize)) {
            return null;
          }
          this.envelope.setRoutes(decoder.getGuidArray());
          this.state = Deserializer.ENVELOPE_RECORD_ROUTES_SIZE;
          decoder.got();
          break;
        }
        case Deserializer.ENVELOPE_RECORD_ROUTES_SIZE: {
          if(!decoder.has(1)) {
            return null;
          }
          this.envelopeRecordRoutesSize = decoder.calculateDynamicBytes();
          this.state = Deserializer.ENVELOPE_RECORD_ROUTES;
          decoder.got();
          break;
        }
        case Deserializer.ENVELOPE_RECORD_ROUTES: {
          if(!decoder.has(this.envelopeRecordRoutesSize)) {
            return null;
          }
          this.envelope.setRecordRoutes(decoder.getGuidArray());
          if(this.hasTextCache) {
            this.state = Deserializer.TEXT_CACHE_SIZE_1;
          }
          else {
            this.state = Deserializer.DATA_SIZE_1;
          }
          decoder.got();
          break;
        }
        /*case Deserializer.ENVELOPE_SERVICE_NAMES: {
          if(!decoder.has(this.envelopeRecordRoutesSize)) {
            return null;
          }
          let size = this.envelopeRecordRoutesSize;
          while(size > 0) {
            const recordRoute = decoder.getGuid();
            this.envelope.recordRoutes.push(recordRoute);
            size -= decoder.guidSize();
          }
          if(this.hasTextCache) {
            this.state = Deserializer.TEXT_CACHE_SIZE_1;
          }
          else {
            this.state = Deserializer.DATA_SIZE_1;
          }
          decoder.got();
          break;
        }*/
        case Deserializer.TEXT_CACHE_SIZE_1: {
          if(!decoder.has(1)) {
            return null;
          }
          this.cachedTextSize = decoder.getUint8();
          if(this.cachedTextSize <= 253) {
            this.state = Deserializer.TEXT_CACHE;
          }
          else if(254 === this.cachedTextSize) {
            this.state = Deserializer.TEXT_CACHE_SIZE_2;
          }
          else if(255 === this.cachedTextSize) {
            this.state = Deserializer.TEXT_CACHE_SIZE_2;
          }
          decoder.got();
          break;
        }
        case Deserializer.TEXT_CACHE_SIZE_2: {
          if(254 === this.cachedTextSize) {
            if(!decoder.has(2)) {
              return null;
            }
            this.cachedTextSize = decoder.getUint16();
          }
          else if(255 === this.cachedTextSize) {
            if(!decoder.has(4)) {
              return null;
            }
            this.cachedTextSize = decoder.getUint32();
          }
          this.state = Deserializer.TEXT_CACHE;
          decoder.got();
        }
        case Deserializer.TEXT_CACHE: {
          if(!decoder.has(this.cachedTextSize)) {
            return null;
          }
          this.cachedTextBuffer = decoder.getSizedBinary(this.cachedTextSize);
          this.state = Deserializer.DATA_SIZE_1;
          decoder.got();
        }
        case Deserializer.DATA_SIZE_1: {
          if(!decoder.has(1)) {
            return null;
          }
          this.dataSize = decoder.getUint8();
          if(this.dataSize <= 253) {
            this.state = Deserializer.DATA;
          }
          else if(254 === this.dataSize) {
            this.state = Deserializer.DATA_SIZE_2;
          }
          else if(255 === this.dataSize) {
            this.state = Deserializer.DATA_SIZE_2;
          }
          decoder.got();
          break;
        }
        case Deserializer.DATA_SIZE_2: {
           if(254 === this.dataSize) {
            if(!decoder.has(2)) {
              return null;
            }
            this.dataSize = decoder.getUint16();
          }
          else if(255 === this.dataSize) {
            if(!decoder.has(4)) {
              return null;
            }
            this.dataSize = decoder.getUint32();
          }
          this.state = Deserializer.DATA;
          decoder.got();
        }
        case Deserializer.DATA: {
          if(!decoder.has(this.dataSize)) {
            return null;
          }
          if(this.isBinary) {
            this.bin = decoder.getSizedBinary(this.dataSize);
          }
          else {
            this.msg = JSON.parse(decoder.getSizedString(this.dataSize), Deserializer.jsonFunc);
          }
          if(this.hasBuffers) {
            this.state = Deserializer.HEADER_BUFFERS_NBR;
          }
          else {
            this.state = Deserializer.DONE;
          }
          decoder.got();
          break;
        }
        case Deserializer.HEADER_BUFFERS_NBR: {
          if(!decoder.has(1)) {
            return null;
          }
          this.nbrOfDataBuffers = decoder.getUint8();
          this.dataBufferSizes = [];
          this.dataBuffers = [];
          this.state = Deserializer.HEADER_BUFFERS_SIZES;
          decoder.got();
        }
        case Deserializer.HEADER_BUFFERS_SIZES: {
          if(!decoder.has(4 * this.nbrOfDataBuffers)) {
            return null;
          }
          for(let i = 0; i < this.nbrOfDataBuffers; ++i) {
            const size = decoder.getUint32();
            this.dataBufferSizes.push(size);
          }
          this.state = Deserializer.DATA_BUFFERS;
          decoder.got();
        }
        case Deserializer.DATA_BUFFERS: {
          if(!decoder.has(this.dataBufferSizes[this.dataBufferSizeIndex])) {
            return null;
          }
          const dataBuffer = decoder.getUint8Array(this.dataBufferSizes[this.dataBufferSizeIndex]);
          this.dataBuffers.push(dataBuffer);
          if(++this.dataBufferSizeIndex === this.dataBufferSizes.length) {
            this.state = Deserializer.DONE;
          }
          decoder.got();
          break;
        }
        case Deserializer.DONE: {
          const coreMessage = new CoreMessage(this.msgId, this.hasEnvelope, this.isBinary, this.hasBuffers, this.hasTextCache, this.isServiceAction, this.envelope, this.msg, this.bin, this.cachedTextBuffer, this.dataBuffers);
          this.clear();
          return coreMessage;
        }
      }
    }
  }
}


module.exports = Deserializer;

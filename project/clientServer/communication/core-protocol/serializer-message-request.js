
'use strict';

const EncoderConst = require('./encoder-const');


class SerializerMessageRequest {
  constructor(jsonFunc) {
    this.jsonFunc = jsonFunc;
    this.encoder = null;
    this.params = [];
  }
  
  init(encoder) {
    this.encoder = encoder;
  }
  
  do(msg, cachedTexts) {
    this.params = [];
    const encoder = this.encoder;
    const size = this._calculateData(msg);
    const buffer = encoder.createBuffer(size);
    encoder.setGuid(msg.id);
    encoder.setUint16(msg.requests.length);
    this._writeRequests(msg, cachedTexts);
    return buffer;
  }
  
  _calculateData(msg) {
    const encoder = this.encoder;
    let size = EncoderConst.GuidSize + EncoderConst.Uint16Size;
    for(let i = 0; i < msg.requests.length; ++i) {
      const request = msg.requests[i];
      size += EncoderConst.Uint16Size + (!msg.isServiceAction ? EncoderConst.CtSize : encoder.getStringBytes(request.name)) + EncoderConst.Uint8Size;
      if(request.params) {
        const params = JSON.stringify(request.params, this.jsonFunc);
        size += encoder.getStringBytes(params);
        this.params.push(params);
      }
      else {
        this.params.push(null);
        size += encoder.calculateDynamicBytes(0);
      }
      if(request.sessionId) {
        size += EncoderConst.GuidSize;
      }
    }
    return size;
  }
  
  _writeRequest(request, cachedTexts, index, isServiceAction) {
    const encoder = this.encoder;
    encoder.setUint16(request.index);
    if(!isServiceAction) {
      encoder.setCtString(request.name, cachedTexts);
    }
    else {
      encoder.setString(request.name);
    }
    if(request.params) {
      const params = this.params[index];
      encoder.setString(params);
    }
    else {
      encoder.setString('');
    }
    if(request.sessionId) {
      encoder.setUint1_0(1, true);
      encoder.setGuid(request.sessionId);
    }
    else {
      encoder.setUint1_0(0, true);
    }
  }
  
  _writeRequests(msg, cachedTexts) {
    msg.requests.forEach((request, index) => {
      this._writeRequest(request, cachedTexts, index, msg.isServiceAction);
    });
  }
}


module.exports = SerializerMessageRequest;

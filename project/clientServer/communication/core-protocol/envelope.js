
'use strict';


class Envelope {
  constructor(serviceName, recordRoute) {
    this.serviceName = serviceName ? serviceName : '';
    this.recordRoutes = recordRoute ? (Array.isArray(recordRoute) ? recordRoute : [recordRoute]) : [];
    this.routes = [];
  }
  
  setServiceName(serviceName) {
    this.serviceName = serviceName;
  }
  
  setRoutes(routes) {
    this.routes = routes;
  }
  
  setRecordRoutes(recordRoutes) {
    this.recordRoutes = recordRoutes;
  }
  
  shiftRoute() {
    return this.routes.shift();
  }
  
  pushRecordRoute(recordRoute) {
    this.recordRoutes.push(recordRoute);
  }
  
  createRouteEnvelope() {
    this.routes = [...this.recordRoutes];
    this.routes.reverse();
  }
}


module.exports = Envelope;

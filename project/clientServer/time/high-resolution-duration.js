
'use strict';


class HighResolutionDuration {
  constructor(duration) {
    const dateRaw = duration;
    const seconds = dateRaw / 1000000000n;
    this.seconds = Number(seconds);
    const dateRest = dateRaw - seconds * 1000000000n;
    this.milliSeconds = Number(dateRest) / 1000000;
  }
    
  getDuration() {
    if(0 !== this.seconds) {
      return `${this.seconds}s ${this.milliSeconds.toFixed(3).toString().padStart(7, '0')}ms`;
    }
    else {
      return `${this.milliSeconds.toFixed(3).toString()}ms`;
    }
  }
}


module.exports = HighResolutionDuration;


'use strict';


class HighResolutionDate {
  constructor(timestamp) {
    const dateRaw = timestamp;
    this.date = new Date(Math.trunc(Number(dateRaw / 1000000n)));
    const dateSeconds = dateRaw / 1000000000n;
    const dateRest = dateRaw - dateSeconds * 1000000000n;
    this.milliSeconds = Number(dateRest) * 0.000001;
  }
    
  getDateMilliSeconds() {
    return `${this.date.toUTCString()} : ${this.milliSeconds.toFixed(3).toString().padStart(7, '0')}`;
  }
  
  static getMilliSeconds(hrtime) {
    return Number(hrtime / 1000000n);
  }
}


module.exports = HighResolutionDate;

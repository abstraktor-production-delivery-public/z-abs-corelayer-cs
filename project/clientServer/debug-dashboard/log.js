
'use strict';

const HighResolutionDate = require('z-abs-corelayer-cs/clientServer/time/high-resolution-date');
const Path = !global.window ? require('path') : undefined;
const Process = !global.window ? require('process') : undefined;



class DebugDashboard {
  static BLACK = 0;
  static WHITE = 1;
  static RED = 2;
  static RED_BRIGHT = 3;
  static GREEN = 4;
  static GREEN_BRIGHT = 5;
  static BLUE = 6;
  static BLUE_BRIGHT = 7;
  static YELLOW = 8;
  static YELLOW_BRIGHT = 9;
  static CYAN = 10;
  static CYAN_BRIGHT = 11;
  static MAGENTA = 12;
  static MAGENTA_BRIGHT = 13;
  static GREY = 14;
  static GREY_BRIGHT = 15;
  static _colors = [
    ['#000000', '\x1b[30m'], //Black
    ['#FFFFFF', '\x1b[97m'], // White
    ['#800000', '\x1b[31m'], //Red
    ['#FF0000', '\x1b[91m'], //RedBright
    ['#008000', '\x1b[32m'], //Green
    ['#00FF00', '\x1b[92m'], //GreenBright
    ['#000080', '\x1b[34m'], //Blue
    ['#0000FF', '\x1b[94m'], //BlueBright
    ['#F6BE00', '\x1b[33m'], //Yellow
    ['#FFFF00', '\x1b[93m'], //YellowBright
    ['#008080', '\x1b[36m'], //Cyan
    ['#00FFFF', '\x1b[96m'], //CyanBright
    ['#800080', '\x1b[35m'], //Magenta
    ['#FF00FF', '\x1b[95m'], //MagentaBright
    ['#808080', '\x1b[90m'], //Gray
    ['#C0C0C0', '\x1b[37m']  //GrayBright
  ];
  
  static funcCreateTime = null;
  static IN_BROWSER = !!global.window;
  static WRITELN_IMPL_INDEX = DebugDashboard.IN_BROWSER ? 0 : (Process.env.VSCODE_INSPECTOR_OPTIONS ? 0 : 1);
  static WRITELN_IMPL = [DebugDashboard.writelnBrowserOrVSCode, DebugDashboard.writelnNodejs];
  
  static MAX_FILENAME_LENGTH = 50;
  static MAX_FILENAME_LENGTH_DOT = DebugDashboard.MAX_FILENAME_LENGTH - 3;
  
  static PAD_FILE_NAME_LENGTH = 50;
  
  static logs = [
    DebugDashboard.logEngine,
    DebugDashboard.logDebug,
    DebugDashboard.logError,
    DebugDashboard.logWarning,
    DebugDashboard.logIp,
    DebugDashboard.logGui,
    DebugDashboard.logVerifySuccess,
    DebugDashboard.logVerifyFailure,
    DebugDashboard.logTestData,
    DebugDashboard.logBrowserLog,
    DebugDashboard.logBrowserErr
  ];
  static setTimeCreator(funcCreateTime) {
    DebugDashboard.funcCreateTime = funcCreateTime;
  }
  
  static black(text) {
    return {
      _text: text,
      _color: DebugDashboard.BLACK
    };
  }
  
  static white(text) {
    return {
      _text: text,
      _color: DebugDashboard.WHITE
    };
  }
  
  static red(text) {
    return {
      _text: text,
      _color: DebugDashboard.RED
    };
  }
  
  static redBright(text) {
    return {
      _text: text,
      _color: DebugDashboard.RED_BRIGHT
    };
  }
  
  static green(text) {
    return {
      _text: text,
      _color: DebugDashboard.GREEN
    };
  }
  
  static greenBright(text) {
    return {
      _text: text,
      _color: DebugDashboard.GREEN_BRIGHT
    };
  }
  
  static blue(text) {
    return {
      _text: text,
      _color: DebugDashboard.BLUE
    };
  }
  
  static blueBright(text) {
    return {
      _text: text,
      _color: DebugDashboard.BLUE_BRIGHT
      };
  }
  
  static yellow(text) {
    return {
      _text: text,
      _color: DebugDashboard.YELLOW
    };
  }
  
  static yellowBright(text) {
    return {
      _text: text,
      _color: DebugDashboard.YELLOW_BRIGHT
    };
  }
  
  static cyan(text) {
    return {
      _text: text,
      _color: DebugDashboard.CYAN
    };
  }
  
  static cyanBright(text) {
    return {
      _text: text,
      _color: DebugDashboard.CYAN_BRIGHT
    };
  }
  
  static magenta(text) {
    return {
      _text: text,
      _color: DebugDashboard.MAGENTA
    };
  }
  
  static magentaBright(text) {
    return {
      _text: text,
      _color: DebugDashboard.MAGENTA_BRIGHT
    };
  }
  
  static grey(text) {
    return {
      _text: text,
      _color: DebugDashboard.GREY
    };
  }
  
  static greyBright(text) {
    return {
      _text: text,
      _color: DebugDashboard.GREY_BRIGHT
    };
  }
  
  static info(...params) {
    const info = DebugDashboard._getInfo();
    DebugDashboard.writeln(...info.timestamp, 'INFO   ', DebugDashboard.yellow(info.lineNumber), ' ', DebugDashboard.cyan(info.fileName), ...params);
  }
  
  static error(...params) {
    const info = DebugDashboard._getInfo();
    DebugDashboard.writeln(...info.timestamp, DebugDashboard.red('ERROR  '), DebugDashboard.yellow(info.lineNumber), ' ', DebugDashboard.cyan(info.fileName), ...params);
  }
  
  static debug(...params) {
    const info = DebugDashboard._getInfo();
    DebugDashboard.writeln(...info.timestamp, DebugDashboard.magenta('DEBUG  '), DebugDashboard.yellow(info.lineNumber), ' ', DebugDashboard.cyan(info.fileName), ...params);
  }
  
  static warning(...params) {
    const info = DebugDashboard._getInfo();
    DebugDashboard.writeln(...info.timestamp, DebugDashboard.yellow('WARNING'), DebugDashboard.yellow(info.lineNumber), ' ', DebugDashboard.cyan(info.fileName), ...params);
  }
  
  // -> LOGS - used by logger.
  
  static log(logType, time, fileName, lineNumber, ...params) {
    DebugDashboard.logs[logType](time, fileName, lineNumber, ...params);
  }
  
  static logEngine(time, fileName, lineNumber, ...params) {
    DebugDashboard.writeln(...DebugDashboard._getLogStart(DebugDashboard.ENGINE, time, fileName, lineNumber, ...params));
  }
  
  static logDebug(time, fileName, lineNumber, ...params) {
    DebugDashboard.writeln(...DebugDashboard._getLogStart(DebugDashboard.DEBUG, time, fileName, lineNumber, ...params));
  }
  
  static logError(time, fileName, lineNumber, ...params) {
    DebugDashboard.writeln(...DebugDashboard._getLogStart(DebugDashboard.ERROR, time, fileName, lineNumber, ...params));
  }
  
  static logWarning(time, fileName, lineNumber, ...params) {
    DebugDashboard.writeln(...DebugDashboard._getLogStart( DebugDashboard.WARNING, time, fileName, lineNumber, ...params));
  }
  
  static logIp(time, fileName, lineNumber, ...params) {
    DebugDashboard.writeln(...DebugDashboard._getLogStart(DebugDashboard.IP, time, fileName, lineNumber, ...params));
  }
  
  static logGui(time, fileName, lineNumber, ...params) {
    DebugDashboard.writeln(...DebugDashboard._getLogStart(DebugDashboard.GUI, time, fileName, lineNumber, ...params));
  }
  
  static logVerifySuccess(time, fileName, lineNumber, ...params) {
    DebugDashboard.writeln(...DebugDashboard._getLogStart(DebugDashboard.VERIFY_SUCCESS, time, fileName, lineNumber, ...params));
  }
  
  static logVerifyFailure(time, fileName, lineNumber, ...params) {
    DebugDashboard.writeln(...DebugDashboard._getLogStart(DebugDashboard.VERIFY_FAILURE, time, fileName, lineNumber, ...params));
  }
  
  static logTestData(time, fileName, lineNumber, ...params) {
    DebugDashboard.writeln(...DebugDashboard._getLogStart(DebugDashboard.TEST_DATA, time, fileName, lineNumber, ...params));
  }
  
  static logBrowserLog(time, fileName, lineNumber, ...params) {
    DebugDashboard.writeln(...DebugDashboard._getLogStart(DebugDashboard.BROWSER_LOG, time, fileName, lineNumber, ...params));
  }
  
  static logBrowserErr(time, fileName, lineNumber, ...params) {
    DebugDashboard.writeln(...DebugDashboard._getLogStart(DebugDashboard.BROWSER_ERR, time, fileName, lineNumber, ...params));
  }
    
  // <- NEW LOGS
  
  // -> PRINTS
  
  static writelnBrowserOrVSCode(...params) {
    const texts = [];
    const csses = [];
    params.forEach((param) => {
      if('object' === typeof param && undefined !== param?._text) {
        texts.push(`%c${param?._text}`);
        csses.push(`color: ${DebugDashboard._colors[param._color][DebugDashboard.WRITELN_IMPL_INDEX]}; background-color: Black;`);
      }
      else {
        texts.push(`%c${param}`);
        csses.push(`color: white; background-color: Black;`);
      }
    });
    console.log(texts.join(''), ...csses);
  }
  
  static writelnNodejs(...params) {
    const texts = [];
    params.forEach((param) => {
      if('object' === typeof param && undefined !== param?._text) {
        texts.push(DebugDashboard._colors[param._color][DebugDashboard.WRITELN_IMPL_INDEX]);
        texts.push(param?._text);
      }
      else {
        texts.push(DebugDashboard._colors[DebugDashboard.WHITE][DebugDashboard.WRITELN_IMPL_INDEX]);
        texts.push(param);
      }
    });
    console.log(texts.join(''), '\x1b[0m');
  }
  
  static writeln(...params) {
    DebugDashboard.WRITELN_IMPL[DebugDashboard.WRITELN_IMPL_INDEX](...params);
  }
  
  static writelnTime(...params) {
    const info = DebugDashboard._getInfo();
    DebugDashboard.WRITELN_IMPL[DebugDashboard.WRITELN_IMPL_INDEX](...info.timestamp, ...params);
  }
  
  static writelnTimeError(err, ...params) {
    const info = DebugDashboard._getInfo();
    DebugDashboard.WRITELN_IMPL[DebugDashboard.WRITELN_IMPL_INDEX](...info.timestamp, ...params, '. ', err);
  }
  
  static print(...params) {
    DebugDashboard.writeln(DebugDashboard.yellow('### '), ...params);
  }
  
  static printTop() {
    if(!DebugDashboard.IN_BROWSER) {
      DebugDashboard.writeln(DebugDashboard.yellow('#############################'));
    }
    else {
      DebugDashboard.writeln('#############################');
    }
  }
  
  static printMiddle() {
    if(!DebugDashboard.IN_BROWSER) {
      DebugDashboard.writeln(DebugDashboard.yellow('#############################'));
    }
    else {
      DebugDashboard.writeln('#############################');
    }
  }
  
  static printBottom() {
    if(!DebugDashboard.IN_BROWSER) {
      DebugDashboard.writeln(DebugDashboard.yellow('#############################'));
    }
    else {
      DebugDashboard.writeln('#############################');
    }
  }
  
  // -> PRINTS
  
  static _getLogStart(logType, time, fileName, lineNumber, ...params) {
    const date = new Date(HighResolutionDate.getMilliSeconds(time));
    const padLineNumber = DebugDashboard.yellow(lineNumber.toString().padStart(3) + ' ');
    const padFileName = DebugDashboard.cyanBright(fileName.length < DebugDashboard.PAD_FILE_NAME_LENGTH ? fileName.padStart(DebugDashboard.PAD_FILE_NAME_LENGTH, ' ') + ' ' : '...' + fileName.substring(fileName.length - DebugDashboard.PAD_FILE_NAME_LENGTH + 3) + ' ');
    return [...DebugDashboard._getTime(date), DebugDashboard.LOG_TYPE_NAME[logType], padLineNumber, padFileName, ...params];
  }
  
  static _getTime(date) {
    return [DebugDashboard.white('['), DebugDashboard.grey(`${date.getHours()}`.padStart(2, '0') + ':' + `${date.getMinutes()}`.padStart(2, '0') + ':' + `${date.getSeconds()}`.padStart(2, '0')), DebugDashboard.grey('.'), DebugDashboard.grey(date.getMilliseconds().toString().padStart(3, '0')), DebugDashboard.white('] ')];
  }
  
  static _getInfo() {
    let date = null;
    if(DebugDashboard.funcCreateTime) {
      date = new Date(HighResolutionDate.getMilliSeconds(DebugDashboard.funcCreateTime()));
    }
    else {
      date = new Date();
    }
    const timestamp = DebugDashboard._getTime(date);
    const sep = !DebugDashboard.IN_BROWSER ? Path.sep : '/';
    let fileName = '';
    let lineNumber = '';
    
    if(Error.captureStackTrace) {
      const origPrepare = Error.prepareStackTrace;
      const origLimit = Error.stackTraceLimit;
      const dummyError = {};
      Error.prepareStackTrace = (error, structuredStackTrace) => {
        const stack = structuredStackTrace[Math.min(3, structuredStackTrace.length - 1)];
        return {
          fileName: stack.getFileName(),
          lineNumber: stack.getLineNumber()
        };
      };
      Error.stackTraceLimit = 3;
      Error.captureStackTrace(dummyError);
      const logInfo = dummyError.stack;
      Error.prepareStackTrace = origPrepare;
      Error.stackTraceLimit = origLimit;
      fileName = logInfo.fileName;
      lineNumber = ('' + logInfo.lineNumber).padStart(3, ' ');
    }
    if(fileName) {
      const lastPathSepIndex = fileName.lastIndexOf(sep);
      if(-1 !== lastPathSepIndex) {
        fileName = fileName.substring(lastPathSepIndex + 1);
        if(DebugDashboard.MAX_FILENAME_LENGTH < fileName.length) {
          fileName = '...' + fileName.substring(fileName.length - DebugDashboard.MAX_FILENAME_LENGTH_DOT);
        }
      }
      fileName = fileName.padEnd(DebugDashboard.MAX_FILENAME_LENGTH, ' ');
    }
    return {
      fileName: fileName,
      lineNumber: lineNumber,
      timestamp: timestamp
    };
  }
}


// DOUBLE-CODE: applayer-actorjs
DebugDashboard.ENGINE = 0;
DebugDashboard.DEBUG = 1;
DebugDashboard.ERROR = 2;
DebugDashboard.WARNING = 3;
DebugDashboard.IP = 4;
DebugDashboard.GUI = 5;
DebugDashboard.VERIFY_SUCCESS = 6;
DebugDashboard.VERIFY_FAILURE = 7;
DebugDashboard.TEST_DATA = 8;
DebugDashboard.BROWSER_LOG = 9;
DebugDashboard.BROWSER_ERR = 10;


DebugDashboard.LOG_TYPE_NAME = [
  DebugDashboard.yellow('ENGINE   '),
  DebugDashboard.magenta('DEBUG    '),
  DebugDashboard.red('ERROR    '),
  DebugDashboard.yellowBright('WARNING  '),
  DebugDashboard.blueBright('IP       '),
  DebugDashboard.blue('GUI      '),
  DebugDashboard.green('SUCCESS  '),
  DebugDashboard.redBright('FAILURE  '),
  DebugDashboard.cyanBright('TEST DATA'),
  DebugDashboard.cyanBright('B-Log    '),
  DebugDashboard.cyanBright('B-Err    ')
];

/*DebugDashboard.writeln(DebugDashboard.black('text'));
DebugDashboard.writeln(DebugDashboard.white('text'));
DebugDashboard.writeln(DebugDashboard.red('text'));
DebugDashboard.writeln(DebugDashboard.redBright('text'));
DebugDashboard.writeln(DebugDashboard.green('text'));
DebugDashboard.writeln(DebugDashboard.greenBright('text'));
DebugDashboard.writeln(DebugDashboard.blue('text'));
DebugDashboard.writeln(DebugDashboard.blueBright('text'));
DebugDashboard.writeln(DebugDashboard.yellow('text'));
DebugDashboard.writeln(DebugDashboard.yellowBright('text'));
DebugDashboard.writeln(DebugDashboard.cyan('text'));
DebugDashboard.writeln(DebugDashboard.cyanBright('text'));
DebugDashboard.writeln(DebugDashboard.magenta('text'));
DebugDashboard.writeln(DebugDashboard.magentaBright('text'));
DebugDashboard.writeln(DebugDashboard.grey('text'));
DebugDashboard.writeln(DebugDashboard.greyBright('text'));*/

global.ddb = DebugDashboard;


'use strict';


class MutexLocalCallback {
  constructor() {
    this.startTime = 0n;
    this.locked = false;
    this.queue = [];
  }
  
  add(object, method) {
    const originalMethod = method.bind(object);
    const self = this;
    return function (...args) {
      if(self.locked) {
        self.queue.push({
          originalMethod: originalMethod,
          args: args
        });
      }
      else {
        const done = args[args.length - 1];
        args[args.length - 1] = (...doneArgs) => {
          if(0 !== self.queue.length) {
            const next = self.queue.shift();
            process.nextTick(next.originalMethod, ...next.args);
          }
          else {
            self.locked = false;
          }
          done(...doneArgs);
        };
        self.locked = true;
        originalMethod(...args);
      }
    };
  }
  
  setStartTime() {
    this.startTime = process.hrtime.bigint();
  }
  
  logTime(...text) {
    console.log('TIME:', (process.hrtime.bigint() - this.startTime) / 1000000n, ...text);
  }
}


module.exports = MutexLocalCallback;

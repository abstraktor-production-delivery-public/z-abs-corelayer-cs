
'use strict';

const GuidGenerator = require('./guid-generator');
const Os = require('os');


class Project {
  constructor(source = [], type = undefined, plugin = false) {
    this.source = source;
    this.type = type;
    this.plugin = plugin;
    this.workspaceName = null;
    this.selectedNode = null;
    this.projectId = GuidGenerator.create();
    this.saved = true;
  }
  
  clone() {
    const project = new Project(this.source, this.type, this.plugin);
    project.selectedNode = this.selectedNode;
    project.projectId = this.projectId;
    project.saved = this.saved;
    return project;
  }
  
  set(source, type, workspaceName) {
    this.source = source;
    this.type = type;
    this.workspaceName = workspaceName;
  }
  
  _importPureChild(node, childKey, nodeDatas, expandeds) {
    const childNodeData = nodeDatas.get(childKey);
    const columns = childNodeData.columns;
    const key = columns[0];
    const type = columns[3];
    if('folder' === type || 'static_folder' === type || 'project_folder' === type) {
      const typesString = columns[5];
      const types = typesString.substring(1, typesString.length - 1).split(',');
      const expanded = expandeds.has(key);
      const childNode = this.addFolderToNode(columns[2], columns[4], types, node, type, key, expanded);
      childNodeData.children.forEach((nextChildKey) => {
        this._importPureChild(childNode, nextChildKey, nodeDatas, expandeds);
      });
    }
    else if('file' === type) {
      this.addFileToNode(columns[2], columns[4], columns[5], node, key);
    }
    else {
      ddb.error('UNKNOWN TYPE:', type);
    }
  }
  
  importPure(pureProject, jsonProject) {
    this.source = [];
    if(!pureProject) {
      return;
    }
    const rows = pureProject.split(Os.EOL);
    rows.forEach((row, index, array) => {
      array[index] = row.trim();
    });
    const nodeDatas = new Map();
    const firstRow = rows[0];
    const firstColumns = firstRow.split(';');;
    const rootNodeData = {
      columns: firstColumns,
      children: []
    };
    nodeDatas.set(firstColumns[0], rootNodeData);
    for(let i = 1; i < rows.length; ++i) {
      const row = rows[i];
      const columns = row.split(';');
      nodeDatas.set(columns[0], {
        columns: columns,
        children: []
      });
    }
    
    // Add children
    nodeDatas.forEach((child) => {
      const parentKey = child.columns[1];
      const parent = nodeDatas.get(parentKey);
      if(parent) {
        parent.children.push(child.columns[0]);
      }
      else {
        if(parentKey) {
          ddb.error('Parent not found. Child dropped.');
        }
      }
    });

    if(1 <= nodeDatas.size) {
      const expandeds = new Set();
      this._getAllExpandedKeys(jsonProject, expandeds);
      const typesString = firstColumns[5];
      const types = typesString.substring(1, typesString.length - 1).split(',');
      const key = firstColumns[0];
      const expanded = expandeds.has(key);
      let node = this.addRootFolder(firstColumns[2], types, key, expanded);
      rootNodeData.children.forEach((childKey) => {
        this._importPureChild(node, childKey, nodeDatas, expandeds);
      });
      this.sortNode(this.source[0]);
    }
  }
  
  sortParent(node) {
    const foundFolder = this.findFolder(this.source, node.data.path.replace(new RegExp('[\\\\]', 'g'), '/'));
    if(foundFolder) {
      this.sortNode(foundFolder);
    }
  }
  
  sortNode(node) {
    node.children.sort((a, b) => {
      if(a.folder !== b.folder) {
        return a.folder ? -1 : 1;
      }
      else {
        return a.title >= b.title ? 1 : -1;
      }
    });
    for(let i = 0; i < node.children.length; ++i) {
      const child = node.children[i];
      if(child.folder) {
        this.sortNode(child);
      }
    }
  }
  
  _getAllExpandedKeys(jsonProject, expandeds) {
    if(jsonProject) {
      jsonProject.forEach((node) => {
        if(node.folder && node.expanded) {
          expandeds.add(node.key);
        }
        this._getAllExpandedKeys(node.children, expandeds);
      });
    }
  }
  
  _exportPureChildren(node, parentKey, result) {
    const value = `${node.key};${parentKey};${node.title};${node.folder ? node.data.type : 'file'};${node.data.path};${node.folder ? '[' + node.data.types.join(',') + ']' : node.data.type}`;
    result.push(value);
    if(node.children) {
      node.children.forEach((child) => {
        this._exportPureChildren(child, node.key, result);
      });
    }
  }
  
  exportPure() {
    const result = [];
    this._exportPureChildren(this.source[0], '', result);
    return result.join(Os.EOL);
  }
  
  isSaved() {
    return this.saved;
  }
  
  setSaved() {
    this.saved = true;
  }
  
  isEmpty() {
    return 0 === this.source.length;
  }
  
  select(key, selected) {
    this.unSelect();
    const foundNode = this.findKey(this.source, key);
    if(foundNode) {
      foundNode.selected = selected;
      this.selectedNode = foundNode;
    }
  }
  
  unSelect() {
    if(this.selectedNode) {
      this.selectedNode.selected = false;
      this.selectedNode = null;
    }
  }
  
  toggle(key, expanded) {
    const foundNode = this.findKey(this.source, key);
    if(foundNode) {
      foundNode.expanded = expanded;
    }
  }
  
  expandPath(path) {
    const pathSplit = path.split('/');
    if(2 >= pathSplit.length) {
      return;
    }
    if('.' !== pathSplit[0]) {
      return;
    }
    let currentFolder = `${pathSplit[0]}/${pathSplit[1]}/${pathSplit[2]}`;
    pathSplit.shift();
    pathSplit.shift();
    pathSplit.shift();
    let foundFolder = this.findFolder(this.source[0].children, currentFolder);
    if(foundFolder) {
      this.source[0].expanded = true;
      foundFolder.expanded = true;
      while(0 !== pathSplit.length) {
        foundFolder.expanded = true;
        currentFolder = `${currentFolder}/${pathSplit[0]}`;
        foundFolder = this.findFolder(foundFolder.children, currentFolder);
        if(!foundFolder) {
          break;
        }
        foundFolder.expanded = true;
        pathSplit.shift();
      }
    }
  }
  
  isChildKey(parentKey, childKey) {
    const parent = this.findKey(this.source, parentKey);
    if(parent) {
      for(let i = 0; i < parent.children.length; ++i) {
        const child = parent.children[i];
        if(child.key === childKey) {
          return true;
        }
      }
    }
    return false;
  }
  
  findKey(children, key) { // MOST LIKELY THAT THE NODE IS CLOSE TO THE ROOT
    for(let i = 0; i < children.length; ++i) {
      if(children[i].key === key) {
        return children[i];
      }
    }
    for(let i = 0; i < children.length; ++i) {
      if(children[i].children) {
        const foundNode = this.findKey(children[i].children, key);
        if(foundNode) {
          return foundNode;
        }
      }
    }
    return null;
  }
  
  addFileToNode(title, dir, type, node, key=null, selected=false) {
    if(node) {
      const file = {
        key: key ? key : GuidGenerator.create(),
        folder: false,
        title: title,
        selected: selected,
        data: {
          path: dir,
          type: type,
          valid: true
        }
      };
      node.children.push(file);
      this.saved = false;
      return file;
    }
  }
  
  addFile(title, dir, type, cb) {
    return this.addFileToNode(title, dir, type, this.findFolder(this.source, dir.replace(new RegExp('[\\\\]', 'g'), '/')));
  }
  
  addFolderToNode(title, dir, types, node, type='folder', key=null, expanded=false, selected=false) {
    if(node) {
      const folder = {
        key: key ? key : GuidGenerator.create(),
        folder: true,
        title: title,
        expanded: expanded,
        selected: selected,
        data: {
          path: dir,
          types: types,
          type: type
        },
        children: []
      };
      node.children.push(folder);
      this.saved = false;
      if(selected) {
        this.unSelect();
        this.selectedNode = folder;
      }
      return folder;
    }
  }
  
  addFolder(title, data, types=null) {
    return this.addFolderToNode(title, data.path, types ? types : data.types, this.findFolder(this.source, data.path.replace(new RegExp('[\\\\]', 'g'), '/')), data.type);
  }
  
  addRootFolder(title, types, key=null, expanded=false, selected=false) {
    const folder = {
      key: key ? key : GuidGenerator.create(),
      folder: true,
      title: title,
      expanded: expanded,
      selected: selected,
      data: {
        path: '.',
        types: types,
        type: 'project_folder'
      },
      children: []
    }
    if(selected) {
      this.unSelect();
      this.selectedNode = folder;
    }
    this.source.push(folder);
    return folder
  }  
  
  removeNode(path, key) {
    const node = this.findNode(path);
    const foundIndex = node.children.findIndex((found) => {
      return found.key === key;
    });
    if(-1 !== foundIndex) {
      node.children.splice(foundIndex, 1);
      this.saved = false;
    }
  }
  
  getRootName() {
    // FIX to be general
    const node = this.source[0];
    return `${node.data.path}/${node.title}`;
  }
  
  getFileNames(dir) {
    const node = this.findFolder(this.source, dir);
    if(node) {
      if(!node.children) {
        node.children = [];
      }
      return node.children.map((childNode) => {
        return `${childNode.data.path}/${childNode.title}`;
      });
    }
    else {
      return [];
    }
  }
  
  findNode(file) {
    const fileData = this._pathParse(file);
    const node = this.findFolder(this.source, fileData.dir);
    if(node) {
      if(node.children) {
        for(let i = 0; i < node.children.length; ++i) {
          if(node.children[i].title === fileData.base) {
            return node.children[i];
          }
        }
      }
      return node;
    }
  }
  
  findFolder(nodes, dir) {
    const normalizedDir = dir;
    const nodeFound = nodes.find((node) => {
      return node.folder && (normalizedDir.startsWith(`${node.data.path}/${node.title}/`) || normalizedDir === `${node.data.path}/${node.title}`);
    });
    if(nodeFound) {
      if(`${nodeFound.data.path}/${nodeFound.title}` === normalizedDir) {
          return nodeFound;  
      }
      else {
        return this.findFolder(nodeFound.children, normalizedDir);
      }
    }
  }
  
  getAllFileChildren(node) {
    const children = [];
    this._getAllFileChildren(node, children);
    return children;
  }
  
  _getAllFileChildren(node, children) {
    node.children.forEach((child)=> {
      if(child.folder) {
        this._getAllFileChildren(child, children);
      }
      else {
        children.push(child);
      }
    });
  }
  
  renameFile(node, newTitle) {
    node.title = newTitle;
    this.saved = false;
  }
  
  renamePathRecursive(node, newTitle) {
    node.title = newTitle;
    this.saved = false;
    node.children.forEach((child) => {
      this._renamePathRecursive(child, `${node.data.path}/${newTitle}`);
    });
  }
  
  _renamePathRecursive(node, path) {
    node.data.path = path;
    if(node.folder) {
      node.children.forEach((child) => {
        this._renamePathRecursive(child, `${node.data.path}/${node.title}`);
      });
    }
  }
  
  _pathParse(file) {
    const parts = file.split('/');
    let lastPart = parts.pop();
    const lastIndex = lastPart.lastIndexOf('.');
    let dir = parts.join('/');
    if('.' === dir) {
      dir = file;
      lastPart = '';
    }
    if(-1 === lastIndex) {
      return {
        dir: dir,
        base: lastPart,
        ext: undefined,
        name: undefined
      };  
    }
    else {
      return {
        dir: dir,
        base: lastPart,
        ext: lastPart.substring(lastIndex),
        name: lastPart.substring(0, lastIndex)
      };
    }
  }
}

module.exports = Project;
